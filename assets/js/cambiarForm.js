function cambiarForm(idRadio) {
    if (idRadio == "alumno") {
        document.getElementById("formulario").src = "./formularioAlumno.html";
    } else if (idRadio == "profesor") {
        document.getElementById("formulario").src = "./formularioProfesor.html";
    } else if (idRadio == "empresa") {
        document.getElementById("formulario").src = "./formularioEmpresa.html";
    }
}