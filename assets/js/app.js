const app = Vue.createApp({
    data() {
        return {
            tipusInteres: '',
            interessosAlumne: [],
            interessosEmpresa: [],
            missatge: 'Formulari enviat',
            formEnviat: false

        };
    },
    methods: {
        enviarForm(event) {
            event.preventDefault();
            this.formEnviat = true;
        }
    }
}).mount('#app');