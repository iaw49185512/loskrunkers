# LosKrunkers

Pasos para iniciar el proyecto (docker nos da error de nginx...)

- Clone del proyecto

- Entrar al directorio portalPracticas

- ```php artisan key:generate``` y ```composer install```

- Acceder a mysql con ```mysql -u root -p``` (cambiar ```.env``` con los datos de usuario root si es necesario)
En nuestro .env tenemos user "root" y contraseña "password"

- CREATE DATABASE workPractices;

- Salimos de mysql (comando ```exit```)

- Hacemos la migracion: ```php artisan migrate```

- Seeders, en el orden mostrado a continuacion:

```php artisan db:seed --class=company_seed```
```php artisan db:seed --class=offers_seed```
```php artisan db:seed --class=users_seed```

Ya creada la base de datos, hacemos un ```php artisan serve```

Al entrar por localhost estaremos en la pagina principal donde habra botones para ir a la pagina de registro pulsando el boton de "Unete" y a la pagina de login pulsando el boton de "Iniciar sesion".

Como no trabajamos con ningun tipo de sesion de usuario, hemos puesto un link en la nav que dirige hacia el listado de ofertas "Ver ofertas".

Cuando estamos en las vistas de ofertas, registro o login habra una nav en comun donde hemos puesto las vistas importantes para dirigirse mas facilmente a ellas.
